package com.atguigu.gmall.pms.dao;

import com.atguigu.gmall.pms.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author yyf
 * @email yyf@atguigu.com
 * @date 2021-06-28 09:12:58
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
