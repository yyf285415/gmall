package com.atguigu.gmall.pms.dao;

import com.atguigu.gmall.pms.entity.SpuCommentEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品评价
 * 
 * @author yyf
 * @email yyf@atguigu.com
 * @date 2021-06-28 09:12:59
 */
@Mapper
public interface SpuCommentDao extends BaseMapper<SpuCommentEntity> {
	
}
