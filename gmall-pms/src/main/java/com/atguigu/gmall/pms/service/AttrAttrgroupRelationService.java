package com.atguigu.gmall.pms.service;


import com.atguigu.core.bean.PageVo;
import com.atguigu.core.bean.QueryCondition;
import com.atguigu.gmall.pms.entity.AttrAttrgroupRelationEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 属性&属性分组关联
 *
 * @author yyf
 * @email yyf@atguigu.com
 * @date 2021-06-28 09:12:58
 */
public interface AttrAttrgroupRelationService extends IService<AttrAttrgroupRelationEntity> {

    PageVo queryPage(QueryCondition params);
}

