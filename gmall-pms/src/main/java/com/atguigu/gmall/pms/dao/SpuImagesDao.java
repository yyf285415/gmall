package com.atguigu.gmall.pms.dao;

import com.atguigu.gmall.pms.entity.SpuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu图片
 * 
 * @author yyf
 * @email yyf@atguigu.com
 * @date 2021-06-28 09:12:59
 */
@Mapper
public interface SpuImagesDao extends BaseMapper<SpuImagesEntity> {
	
}
