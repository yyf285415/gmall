package com.atguigu.gmall.sms.dao;

import com.atguigu.gmall.sms.entity.SpuFullReductionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品满减信息
 * 
 * @author yyf
 * @email yyf@atguigu.com
 * @date 2021-06-29 09:17:48
 */
@Mapper
public interface SpuFullReductionDao extends BaseMapper<SpuFullReductionEntity> {
	
}
