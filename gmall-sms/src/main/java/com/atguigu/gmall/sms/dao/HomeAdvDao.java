package com.atguigu.gmall.sms.dao;

import com.atguigu.gmall.sms.entity.HomeAdvEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 首页轮播广告
 * 
 * @author yyf
 * @email yyf@atguigu.com
 * @date 2021-06-29 09:17:47
 */
@Mapper
public interface HomeAdvDao extends BaseMapper<HomeAdvEntity> {
	
}
