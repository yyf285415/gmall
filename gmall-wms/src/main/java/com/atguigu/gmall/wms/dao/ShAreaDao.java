package com.atguigu.gmall.wms.dao;

import com.atguigu.gmall.wms.entity.ShAreaEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 全国省市区信息
 * 
 * @author yyf
 * @email yyf@atguigu.com
 * @date 2021-06-29 09:32:53
 */
@Mapper
public interface ShAreaDao extends BaseMapper<ShAreaEntity> {
	
}
