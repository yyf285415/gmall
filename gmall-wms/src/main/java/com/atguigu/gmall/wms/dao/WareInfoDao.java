package com.atguigu.gmall.wms.dao;

import com.atguigu.gmall.wms.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author yyf
 * @email yyf@atguigu.com
 * @date 2021-06-29 09:32:53
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
