package com.atguigu.gmall.wms.dao;

import com.atguigu.gmall.wms.entity.WareOrderTaskEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存工作单
 * 
 * @author yyf
 * @email yyf@atguigu.com
 * @date 2021-06-29 09:32:53
 */
@Mapper
public interface WareOrderTaskDao extends BaseMapper<WareOrderTaskEntity> {
	
}
