package com.atguigu.gmall.ums.dao;

import com.atguigu.gmall.ums.entity.MemberLevelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员等级
 * 
 * @author yyf
 * @email yyf@atguigu.com
 * @date 2021-06-29 09:28:16
 */
@Mapper
public interface MemberLevelDao extends BaseMapper<MemberLevelEntity> {
	
}
